# Stefan's CS 51 Code Reviews
## 2/7

### Git
- [interactive tutorial](https://bitbucket.org/srajkovic/cs51-git-tutorial)
- GitHub has an [awesome in browser example!](https://try.github.io/levels/1/challenges/1)
- `.gitignore` to ignore certain files. Theres on in the base of this repo, that ignore OCaml build artifacts

### Text Editor
- command line (primarily, also available as GUI)
  - emacs
    - I like it because its super extensible
      - syntax highlighting
      - on the fly compiling, showing errors in red
      - can send lines directly to toplevel without copy and paste
  - vim
    - super fast movement, using keystrokes strung together
- GUI
  - TextEdit
    - messes up quotes sometimes
    - no syntax highlighting
  - Sublime
    - decent amount of plugins
  - Atom
    - open sourced by GitHub, decent plugins I think
  - VSCode
    - open sourced by Microsoft, no idea of plugin landscape

### Lab0 review
See `1/cr1_code_examples.ml`. My comments are in `(** *)`, notice the double asterisk

### Other notes:
- Options
  - C malloc returns NULL on failure or an address on success
  - OCaml malloc would return `None` on failure, or `Some address` on success
  - Enforces checking for these error conditions in type system
  ```
  match x with
  | Some v -> do_whatever
  | None -> do_something_else
  ```
- Lists

  ```
  match lst with
  | hd :: hd2 :: tl -> do_something
  ```
  - if you use it you need to include a case that looks like hd :: [], to handle 2 element lists
- matching on multiple values:

  ```
  match x, y with
  | [], [] -> ...
  | hdx :: tlx, [] -> ..
  ```
- Tuples
  - assume x has type (`int list * int list`)
  - `a,b = x` (now a is the first `int list`, and b is the second)
- Compiling and Running
  - from the command line, run `make` (in your directory with `ps1.ml`) to compile your work
  - run `./ps1.byte` to run it
  - Then run `./ps1_tests.byte` to run the tests (that you should be writing!)

## 2/14

### Lab2 review
- curry vs uncurry, and why do we care?
  - curried
    - `f x y`
    - partial application
  - uncurried
    - `f (x,y)`
    - efficiency
- records
  - like structs in C.

### PS1 review
see `2/cr2_code_samples.ml`. My comments are in `(** *)`. Note the second asterisk at the start

### PS2 prep
- `mapfold.ml`
  - HOF
    - `List.fold_right`
    - `List.fold_left`
    - `List.map`
    - `List.filter`
    - A bunch of others too, see the docs (linked below)
  - Also use other like `List.length`, but don't use `List.concat` for
    the one that wants you to implement `List.concat`.
  - `ListLabeled` works the same way, but with labeled arguments should that suit you
  - I've included `2/list.ml` which is the actual List module from the latest Ocaml release.
    - I don't think there's anything in there that you guys can't understand in terms of syntax. 
- `expression.ml`
  - Algebraic Data Types
    - definition
    ```
    type expression =
      | Var
      | Num of float
      ...
    ```
    - match cases
    ```
      match e with
      | Var ->
      | Num x ->
      ...
    ```
  - built-in functions
    - [Read the docs here](http://caml.inria.fr/pub/docs/manual-ocaml/libref/index.html)
      - If something says "since 4.02.3", ignore it, since we're using 4.02.2
    - anything in Pervasives you can reference just by the name after the dot.
      - e.g. `floor`, `sin`, etc.
      - imagine an `open Pervasives` at the top of every file.
        (see code_review.ml for description of open)
  - `Makefile`
    - let's you write the compilation command once, and run it more easily from the command line.
    - See your `ps1` repo for an example, which should be enough for you to write one for ps2.
      - rather than targets `ps1` and `ps1_tests`, you might want `mapfold` and `mapfold_tests`
    - Ask me if you have any questions about them!
    - They'll save you so much time, if you use them, and run your code and tests like that, rather than copy and pasting
      